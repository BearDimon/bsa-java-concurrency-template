package bsa.java.concurrency.fs;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

@Component
public class FileSystemImpl implements FileSystem {
    @Override
    public CompletableFuture<String> saveFile(Path path, byte[] file) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Files.createDirectories(path.getParent());
                if (!Files.exists(path))
                    Files.createFile(path);
                try (FileOutputStream fos = new FileOutputStream(path.toAbsolutePath().toString())) {
                    fos.write(file);
                }
                return path.getFileName().toString();
            } catch (IOException e) {
                return null;
            }
        });
    }

    @Override
    public CompletableFuture<Void> delete(Path path) {
        return CompletableFuture.runAsync(() -> {
            if (!Files.exists(path)) return;
            try {
                if (Files.isDirectory(path))
                    FileUtils.deleteDirectory(path.toFile());
                else
                    Files.delete(path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public CompletableFuture<byte[]> getFile(Path path) {
        return CompletableFuture.supplyAsync(() -> {
            if (!Files.exists(path)) return null;
            try {
                return Files.readAllBytes(path);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        });
    }


}

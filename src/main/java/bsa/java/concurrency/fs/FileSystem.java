package bsa.java.concurrency.fs;

import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
public interface FileSystem {
    CompletableFuture<String> saveFile(Path path, byte[] file);
    CompletableFuture<Void> delete(Path path);
    CompletableFuture<byte[]> getFile(Path path);
}

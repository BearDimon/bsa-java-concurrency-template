package bsa.java.concurrency.image.model;

import lombok.*;

import javax.persistence.*;
import java.net.URL;
import java.util.UUID;

@Entity
@Data
@Table(name = "images")
@AllArgsConstructor
@NoArgsConstructor
public class Image {
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private long dHash;

    @Column
    private URL url;

}

package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<Image, UUID> {
    @Query(value = "SELECT * FROM (" +
                "SELECT " +
                "cast(id AS varchar) AS imageId, " +
                "1 - (CAST(LENGTH(REPLACE(CAST (   ( (CAST(d_hash AS bit(64))) # (CAST(:hash AS bit(64)))  )   AS text), '0', '')) AS float4) / 64) AS matchPercent, " +
                "url AS imageUrl " +
                "FROM images) as search " +
            "WHERE search.matchPercent >= :threshold", nativeQuery = true)
    List<SearchResultDTO> search(@Param("hash") long hash, @Param("threshold") double threshold);
}

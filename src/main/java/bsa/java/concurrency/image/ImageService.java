package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.model.Image;
import bsa.java.concurrency.image.utils.DHasher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class ImageService {

    @Value("${storage.path}")
    private String path;

    private final FileSystem fileSystem;
    private final DHasher dHasher;
    private final ImageRepository imageRepository;

    public ImageService(FileSystem fileSystem, DHasher dHasher, ImageRepository imageRepository) {
        this.fileSystem = fileSystem;
        this.dHasher = dHasher;
        this.imageRepository = imageRepository;
    }

    public void batchUploadImages(MultipartFile[] files, URL baseUrl) {
        try {
            Arrays.stream(files)
                    .map(f -> saveImage(f, baseUrl))
                    .forEach(CompletableFuture::join);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private CompletableFuture<String> saveImage(MultipartFile file, URL baseUrl) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                var bytes = file.getBytes();
                var uuid = UUID.randomUUID();
                var img = new Image(uuid, dHasher.calculateHash(bytes), new URL(baseUrl, uuid.toString()));
                var future = fileSystem.saveFile(Paths.get(path, img.getId().toString()), bytes);
                imageRepository.save(img);
                return future.join();
            } catch (Exception e) {
                return null;
            }
        });
    }


    public List<SearchResultDTO> searchMatches(MultipartFile file, double threshold, URL baseUrl) {
        try {
            var bytes = file.getBytes();
            var hash = dHasher.calculateHash(bytes);
            var result = imageRepository.search(hash, threshold);
            if (!result.isEmpty())
                return result;
            else {
                saveImage(file, baseUrl);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void delete(UUID imageId) {
        fileSystem.delete(Paths.get(path, imageId.toString()));
        imageRepository.deleteById(imageId);
    }

    public void deleteAll() {
        fileSystem.delete(Paths.get(path));
        imageRepository.deleteAll();
    }

    public byte[] getImage(UUID imageId) {
        return fileSystem.getFile(Paths.get(path, imageId.toString())).join();
    }
}
